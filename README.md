# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This demo illustrates and completes the full algorithm for a fingerprint authentication.
This file is a sample demo of the complete fingerprint authentication algorithm hypothesis for a combination of techniques mentioned 
throughout point pattern matching and pattern recognition schemes in research.  Please see the introduction for more details.
For demo purposes this file will either return a 'match' or 'no match' between the pair of fingerprints 1-1 and 2-2.
Note: This demo uses all of the modules in the code collection in one function to determine match or not. Hence, please be sure to import
all codes in this repository.

* Version: Currently as of Oct 2, 2020

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

**Configuration instructions**

All python files are configured to run on the downloaded Python 3.7.0 on a Mac OS system. I've also used the IPython 7.8.0 -- An enhanced Interactive Python on the downloaded Anaconda IDE.

**Installation instructions**

Each module can be downloaded as a separate Python file. However, some modules are dependent on one another and require certain Python libraries as follows:
Independent Modules: 
Fingerprint - imports os.path, Path, numpy, pandas
merge - imports numpy
Candidates - imports math, numpy, pandas, itertools, time, sys
Dependent Modules: 
graph - imports fingerprint, Delaunay, pandas, numpy, matplotlib, pathlib

**Operating instructions**

*Preliminary Instructions:*
	
	▪	You will need to identify your current file path to run the demo (i.e /Users/username/directory/folder/FingerprintData/) to allocate where your ‘raw_data’ folder will be stored. 
	
	▪	Make sure to save the 1-1.xyt and 1-2.xyt files in a folder named ‘raw_data’.
	
	▪	Use the demo file to understand steps for building your own fingerprint authentication file using the algorithm method proposed. 
	
To Run the Demo Instructions:
	
	1.	Python should be downloaded onto your operating system
	
	2.	Choose the IDE of personal preference/choice or Python’s IDLE to run codes.
	
	3.	After all module files are downloaded from GitHub: fingerprint, graph, candidates, and merge, then user should be able to open the ‘demo’ file and run the algorithm using the raw data sample files. Make sure all modules are saved in the same folder directory as the demo file.
	
	4.	Open the demo file on a Python IDLE or personal IDE. (IDLE is an integrated development environment for Python)
	
	5.	Run demo python file
	
	6.	type “FA()” into the console to activate the fingerprint authentication algorithm
	
	7.	User is prompted in console: “Please provide your filepath to the "raw_data" folder:” and enter the file path mentioned in the preliminary instructions.
	
	8.	User is next prompted in console for fingerprint information: (We will enter 1,1, and 1, 2 as these i-j values correspond to the raw data saved in preliminary instructions).
	- Define fingerprint 1 of person i with image j from raw data.
	- Enter person i: 1
	- Enter i'th person's image j: 1
	- Define fingerprint 2 of person i with image j from raw data.
	- Enter person i: 1
	- Enter i'th person's image j: 2
	
	9. Allow algorithm to run.
	
	*a) As of 9-29-20, if fingerprints need to continue to merging triangles - this will be updated in the next release. For now, it only gives notification for if a merge should occur and whether it should continue to the next round (with integer).*
	
	*b) If fingerprints do NOT match, algorithm will detect this at this point.
Note: All graphics and visualization should help when running the algorithm to see each steps intuition.

**A file manifest (list of files included):**

	⁃	fingerprint.py
	⁃	graph.py
	⁃	candidates.py
	⁃	merge.py
	⁃	demo.py
	⁃	1-1.xyt
	⁃	1-2.xyt

**Copyright and licensing information**

There is no purchased/registered copyright or license on this code collection. However, all rights of ownership and these original documentation of codes, original and updates of uploaded codes, all files and scripts are reserved to Catherine I. Berrouet. 
# Copyright 2020, Catherine I. Berrouet, All rights reserved.

**Contact information for the distributor or programmer**

Please contact catherineberrouet@gmail.com, owner and created of this code collection.

**Known bugs**

As of 9.29.20 there are no bugs to the code. There are only MAY BE undefined errors for if statements without an else clause. These functions in particular should always be used when parameters are not Null or None values. Thus, it should not prohibit your code from working.

**Troubleshooting**

Please contact catherineberrouet@gmail.com for any updates/changes/recommendations or suggestions to the code.
There are several comments through out the code to guide users for computations and meaningful optional “print” areas where data analysis can be done for research and verification.

**Credits and acknowledgments**

All rights and credits are reserved to Catherine I. Berrouet. This algorithm was built in an attempt to conduct a thesis project for biometric cryptographic research with Dr.Karabina at the Florida Atlantic University. I would like to acknowledge him for his guidance and insight on proposing an idea to enter the field.  I would also like to give gratitude for those individuals who have collaborated with me along the way to answer my questions in regards to coding and data structures: Ralph Gehy, Shaun Miller, Ian Mallarino, Geordi Taylor.  I would also like to give a special thank you to my father as well as my friends and family at the Outlaw & Company Family Office.

**A changelog (usually for programmers)**

All notible changes to this project will be documented in this file and in this section.
All finalized and uploaded clean code for first release and documentation completed on 9-30-2020.

**A news section (usually for users)**

NA


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
Please contact Catherine Berrouet via email catherineberrouet@gmail.com