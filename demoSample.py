#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: catherineberrouet
Copyright 2020, Catherine I. Berrouet, All rights reserved.
# This sample of code serves as a public statement of proof the fingerprint authentication algorithm developed.
# While the licensure and copyrights are being processed, this will serve as an online template placeholder until the code is publicly released.
# Thank you for your patience. - B., Catherine

This is not a module. It is a demo that you can run upon downloading the entire code collection for the provided two data set files.
(All data files are contained in this GitHub Repository)

This demo illustrates and completes the full algorithm for a fingerprint authentication.
This file is a sample demo of the complete fingerprint authentication algorithm hypothesis for a combination of techniques mentioned 
throughout point pattern matching and pattern recognition schemes in research.  Please see the introduction for more details.
For demo purposes this file will either return a 'match' or 'no match' between the pair of fingerprints 1-1 and 2-2.
Note: This demo uses all of the modules in the code collection in one function to determine match or not. Hence, please be sure to import
all codes in this repository.
"""
# Import Modules
import fingerprint
import graph
import time
import merge
from fingerprint import FingerprintData
fingerprint = FingerprintData()

# Initialization function to prompt user
def define():
    i = int(fingerprint.get_user_input("Enter person i: "))
    j = int(fingerprint.get_user_input("Enter i'th person's image j: "))
    return i,j

# Fingerprint Authentication Algorithm
def FA():
    """ This function takes upon input a pair of fingerprint miniutae points scanned from
    an outside source within a 2-D Euclidean space. This function serves to authenticate
    whether the pair of fingerprints match or detects if they do not match.
    :param fingerprint1:
    :param fingerprint2:
    :return: a string, "Match authenticated" or "Not a match. Do not authenticate"
    """
    start = time.perf_counter()

    # Input: fingerprint minitiae for each fingerprint and construct Delaunay Triangulation DT
    # Asks user for their file path then locates file with parameters below
    user_filepath = fingerprint.get_user_input("Please provide your filepath to the \"raw_data\" folder: ")
    print('')
    print('Define fingerprint 1 of person i with image j from raw data.')
    Mi,Mj = define()
    print('')
    print('Define fingerprint 2 of person i with image j from raw data.')
    Wi,Wj = define()
    print('')

    # Fingerprint 1
    raw_data_file_path = fingerprint.data_file_path(user_filepath, "raw_data/")
    text_data_file_path = fingerprint.data_file_path(user_filepath, "text_files/")
    find_raw_data_file = fingerprint.find_file(raw_data_file_path, Mi, Mj)
    open_raw_data_file = fingerprint.open_file(find_raw_data_file)
    create_new_file_for_storing_data = fingerprint.create_text_file(Mi, Mj)
    new_file = fingerprint.copy_data_to_text(open_raw_data_file, text_data_file_path + create_new_file_for_storing_data)
    initial_array_sets_for_triangulation = fingerprint.create_arrays_from_text(new_file)
    fingerprint1_initial = (initial_array_sets_for_triangulation)
    
    # Fingerprint 2
    raw_data_file_path = fingerprint.data_file_path(user_filepath, "raw_data/")
    text_data_file_path = fingerprint.data_file_path(user_filepath, "text_files/")
    find_raw_data_file = fingerprint.find_file(raw_data_file_path, Wi, Wj)
    open_raw_data_file = fingerprint.open_file(find_raw_data_file)
    create_new_file_for_storing_data = fingerprint.create_text_file(Wi, Wj)
    new_file = fingerprint.copy_data_to_text(open_raw_data_file, text_data_file_path + create_new_file_for_storing_data)
    initial_array_sets_for_triangulation = fingerprint.create_arrays_from_text(new_file)
    fingerprint2_initial = (initial_array_sets_for_triangulation)

    fingerprint1 = graph.dt_dataframe(Mi,Mj) # fingerprint f1-f1
    fingerprint2 = graph.dt_dataframe(Wi,Wj) # fingerprint f2-f2
    
    # Construction of Rotational-Invariant-Tuples of triangles in DT
    import candidates
    dataframe1 = candidates.dataframe_with_rit(fingerprint1)
    dataframe2 = candidates.dataframe_with_rit(fingerprint2)
    
    # Delaunay Triangulation graphs
    fingerprint1_graph = graph.dt(Mi, Mj, 0)
    fingerprint2_graph = graph.dt(Wi, Wj, 0)
    
    # Set threshold value t using empirical data
    threshold_list = [i for i in range(0,800,50)]
    # To print out full chart of empirical calculations - recomment in lines 234-235 of candidates.total_empirical() function
        # Comparing fingerprints 1 and 3
    total_empirical_for_fingerprint1_and_fingerprint2 = candidates.total_empirical(dataframe1, dataframe2, threshold_list) 
    print('Computed threshold values empirically:') 
    t = total_empirical_for_fingerprint1_and_fingerprint2
    print('For fingerprint', Mi,'-',Mj, 'and fingerprint,', Wi, '-', Wj, ': t = ', t)
    # Compute candidate sets using threshold hold value t
    candidates_fingerprint1_and_fingerprint2 = candidates.all_cand_sets(dataframe1, dataframe2, total_empirical_for_fingerprint1_and_fingerprint2)
    print('Candidates computed for ', Mi, '-', Mj,'and fingerprint', Wi, '-', Wj,)
    print(candidates_fingerprint1_and_fingerprint2)
    
    # Conduct condition checks (notated as cc) for the candidate sets generated - finding isomorphic pairs in each triangulation
    cc_fingerprint1_fingerprint2 = candidates.check(dataframe1, dataframe2, total_empirical_for_fingerprint1_and_fingerprint2)
    
    print('Output for condition check on fingerprint ', Mi,'-',Mj, 'and fingerprint,', Wi, '-', Wj,':')
    print('From ', Mi,'-',Mj,': we have', len(cc_fingerprint1_fingerprint2[0]),'pairs found.')
    print(cc_fingerprint1_fingerprint2[0])
    print('From ', Wi,'-',Wj,': we have', len(cc_fingerprint1_fingerprint2[1]),'pairs found.')
    print(cc_fingerprint1_fingerprint2[1])
    print('------------------------------------------------------------------')
    
    # Graph isomorphic triangle pairs found after condition check function
    # From 1-1
    M1, M2 = cc_fingerprint1_fingerprint2[0][0][0], cc_fingerprint1_fingerprint2[0][0][1]
    graph.pair(M1,M2,Mi,Mj)
    # From 2-2
    W1, W2 = cc_fingerprint1_fingerprint2[1][0][0], cc_fingerprint1_fingerprint2[1][0][1]
    graph.pair(W1,W2,Wi,Wj)
    
    # Merge/Find Maximal Cliques
    # Step 1: We find the repeated pairs for each dataset's conditional check output
    round1_dt1 = merge.repeat(cc_fingerprint1_fingerprint2[0])
    round1_dt2 = merge.repeat(cc_fingerprint1_fingerprint2[1])    
    # Step 2:
    # What constitutes a match?
    # What if our set only contains one pair each from condition checks?
    # Then our maximum sized cliques are equal. How do we determine the match? We would have to check the similarity measure again?
    
    #if round1_dt1 is None:
    #    print('use last measure function')
    #elif len(round1_dt1)> 2:
    #    print('start rounds 2 function')
    
    # Output: match or no match
    if(merge.round1(round1_dt1)) == 0:                      # if there are no repeats after condition check
        # for dataset 1
        for triangle_pair in cc_fingerprint1_fingerprint2[0]:
            # get rit's
            t1_rit_0 = fingerprint1['rit'][triangle_pair[0]] 
            t2_rit_0 = fingerprint1['rit'][triangle_pair[1]]
        # for dataset 2
        for triangle_pair in cc_fingerprint1_fingerprint2[1]:
            t1_rit_1 = fingerprint1['rit'][triangle_pair[0]] 
            t2_rit_1 = fingerprint1['rit'][triangle_pair[1]]    
        # last similarity measurement
        import candidates
        similarity1 = candidates.rit_distance(t1_rit_0, t1_rit_1)
        similarity2 = candidates.rit_distance(t2_rit_0, t2_rit_1)
        is_similar = abs((similarity1 - similarity2))        
        # Algorithm completed with recorded time complexity tracked
        end = time.perf_counter()
        Time_passed = (end-start)/60
        print('Total Time Passed for Completed Demo:', Time_passed, 'minutes.')  
      
        if is_similar > t:
            return 'Match authenticated.'
        else:
            return 'Not a match. Do not authenticate.'
    else: # merge.round1(round1_dt1)) == 1                  # there are at least 2 repeats
        print('we continue to merge in rounds for', merge.round1(round1_dt1))